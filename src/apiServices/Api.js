import axios from 'axios';
import Vue from 'vue';
import { router } from '../router';
import store from '@/store';
import {
  BASEURL
} from "@/config";

import constantMessage from '@/helpers/constantMessage';




export default () => {
  const instance = axios.create({
    baseURL: `${BASEURL}`
  })

  window.localStorage.getItem("AUTH_KEY") ?
    (instance.defaults.headers.common["Authorization"] = window.localStorage.getItem("AUTH_KEY")) : undefined;
  instance.defaults.headers.common["Access-Control-Allow-Origin"] = "*";

  instance.interceptors.response.use(function (response) {
    // Do something with response data
    // console.log('response :>> ', response);
    return response;
  }, function (error) {
    //Do something with response error
    if (!error['response']) {
      if (process.env.NODE_ENV !== "development") {
        showErrorMessage(constantMessage.connectivity)
        // redirect to auth page or login again
        store.dispatch("LogoutFunction")
        // window.location.replace("login");
      }
    } else if (error.response.status == 403) {
      if (process.env.NODE_ENV !== "development") {
        showErrorMessage(constantMessage.AuthError);
        // redirect to auth page or login again
        store.dispatch("LogoutFunction")
        // window.location.replace("login");
      }

    } else if (error.response.status == 401) {
      if (process.env.NODE_ENV !== "development") {
        showErrorMessage(constantMessage.AdminError)
        // redirect to auth page or login again
        store.dispatch("LogoutFunction")
        // window.location.replace("login");
      }
    } else {
      showErrorMessage(constantMessage.AdminError)
    }
    return Promise.reject(error);
  });
  return instance;
}
const showErrorMessage = (message) => {
  store.dispatch("AppSetting/showDevErrorNotification", {
    message
  })
}
