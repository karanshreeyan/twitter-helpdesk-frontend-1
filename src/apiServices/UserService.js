import Api from './Api';
import {
    BASEURL
} from "@/config";
import axios from 'axios'


export default {
    GetTweets() {
        return Api().get(`/api/tweets`)
    },
}
