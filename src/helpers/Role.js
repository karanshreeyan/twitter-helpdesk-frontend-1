export const Role = {
    SuperAdmin: 'Super Admin',
    HoManager: 'HO Manager',
    CinemaManager: 'Cinema Manager',
    PosOperator: 'POS Operator',
    // PosOperator: 'POS Operator',
    PosFnBOperator: 'FnB POS Operator',
    Distributor: 'Distributor',
    BoxOfficeFnbOperator: 'Box Office Fnb'
}