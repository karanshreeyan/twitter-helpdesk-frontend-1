import Vue from 'vue'
import moment from "moment";
import cookieType from "@/config/cookieType";

export const removeCookie = () => {
    Object.keys(cookieType).map((x) => {
        Vue.cookie.delete(cookieType[x])
    })
    return "Done";
};


export const addAllKeyWordsIntoArrays = (array, key) => {
    if (array.length > 0) {
        let Obj = { ...array[0] };
        Object.keys(Obj).map((x) => { Obj[x] = null; key ? Obj[key] = 'All' : '' });
        return [Obj].concat(array);
    } else return array;
};

export const addAllKeyWordsIntoArrays_2 = (array, key) => {
    if (array.length > 0) {
        let Obj = { ...array[0] };
        Object.keys(Obj).map((x) => { Obj[x] = null; key ? Obj[key] = 'All' : '' });
        return [Obj].concat(array);
    } else return array;
};

export const generateRandomCode = (max) => {
    return Math.floor(Math.pow(10, max - 1) + Math.random() * (Math.pow(10, max) - Math.pow(10, max - 1) - 1));
    // return Array(max).fill('').reduce((accumulator) => accumulator += Math.floor(Math.random() * (max - 2) + 2), '');
};

export const removeDuplicates = (originalArray, prop) => {
    var newArray = [];
    var lookupObject = {};

    for (var i in originalArray) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for (i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
    return newArray;
};

export const movieWeekEnd = (date) => {
    const dayINeed = 5;
    let startDate = moment(date);
    let FinalDate = null;
    if (startDate.isoWeekday() === 5) {
        startDate.add(1, 'days');
    };
    while (startDate.isoWeekday() !== dayINeed) {
        let d = startDate;
        FinalDate = d.format();
        startDate.add(1, 'days');
    };
    return FinalDate;
};