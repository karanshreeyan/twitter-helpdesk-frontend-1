export default {
    AdminError: "Sorry! Something went wrong. Please Login to continue.",
    connectivity: "Check your internet connectivity or may be right now server is down. kindly contact your administrator",
    AuthError: "Your authorization token is invalid or expired",
    NoRecords: "No Records Found"
}