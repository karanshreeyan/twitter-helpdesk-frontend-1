export default [
    { flag: 'us', language: 'en', title: 'English' },
    { flag: 'es', language: 'es', title: 'Español' }
]