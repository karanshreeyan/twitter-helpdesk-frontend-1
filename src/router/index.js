import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('../pages/Login.vue')
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../pages/Home.vue')
  },
  {
    path: '/loading',
    name: 'Loading',
    component: () => import('../pages/Loading.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let access_token = window.localStorage.getItem("AUTH_KEY")
  let user_id = window.localStorage.getItem("USER_ID")
  console.log('typeof(user_id) :>> ', typeof (user_id));
  if ((!access_token || !user_id || String(user_id).length == 0) && to.path != '/' && to.path != '/loading') {
    next('/')
  } else if (access_token && user_id && String(user_id).length > 0 && to.path != '/home') {
    next('/home')
  } else {
    next()
  }
})

export default router
