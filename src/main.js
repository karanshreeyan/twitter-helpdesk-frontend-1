import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import SocketIO from "socket.io-client";
import VueSocketIO from 'vue-socket.io'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import {
  BASEURL
} from "@/config";


//CSS
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//Used Plugins
Vue.use(BootstrapVue);

// Vue.use(new VueSocketIO({

//   debug: true,
//   connection: SocketIO(BASEURL), //options object is Optional
//   vuex: {
//     store,
//     actionPrefix: "SOCKET_",
//     mutationPrefix: "SOCKET_"
//   },
//   transports: ['websocket'], upgrade: false
// })
// );

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
